## Application Blueprint for Mod9 ASR Engine
### What is Mod9 ASR?
The Mod9 Automatic Speech Recognition (ASR) Engine consists of a Linux server, along with compatible models and support software, that enables clients to send commands and audio over a network and receive the written transcript of what was said in the audio.<br />
[Here is the link to application website home page](https://mod9.io/).<br />
[This application blueprint uses the Mod9's "asr" Docker image](https://hub.docker.com/r/mod9/asr).

### Filling out a deployment blueprint
For the general steps to deploy an application with Unfurl Cloud, see our [Getting Started guides](https://unfurl.cloud/help#guides).

#### Details
To simplify the deployment process, we have set default values for several required environment variables and hidden them from the UI. The remaining environment variables are exposed as the following “Detail” fields, which you must fill in after selecting a deployment: <br />

| Input Name | Default Value |
| ------ | ------ |
| asr_engine_port | 9900 |
| asr_engine_host | 192.168.43.237 |
        
Below is a list of the hidden default environment variables and their values. As explained above, you will not encounter these environment variables as you fill in the deployment blueprint:

| Input Name | Default Value |
| ------ | ------ |
| asr_engine_port | 9900 |

#### Components
Each application blueprint includes **components**. These are the required and optional resources for the application. In most cases, there is more than one way to fulfill a component requirement. After you select a deployment blueprint, you will be prompted to fulfill the component requirements and configure the deployment to your needs. Common components include Compute, DNS, Database, and Mail Server:<br /> 

#### Required vs “Extras”
Some components are required for a deployment to succeed. Others, found under the “Extras” tab, are optional resources that enhance the features or performance of your deployment.
